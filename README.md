Place where I'll be uploading schematics and tutorials for my arduboy esp8266 project: https://gitlab.com/Miguel-hrvs/arduboy-esp8266-adding-games-and-libraries/-/tree/dev?ref_type=heads

You can create mockups to make your own arduboy with gimp using the component part photos in this repo. Then, you can wire them with inkscape.